<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Util;

use Eccube\Application;

class LogUtil
{
    /**
     * ログファイル書き込みを行う
     *
     * @param string $text
     * @param string $filename
     */
    public function putFile($text, $file_name)
    {

        try {
            $fp = fopen($file_name, 'a');//新規作成可・オープン時に削除しない
        } catch (\Exception $e) {
            throw $e;
        }

        if (!flock($fp, LOCK_EX)) {
            throw new \Exception("ファイルロックできませんでした");
        }
        fwrite($fp, $text . "\n"); //ファイルの中身を出力
        fflush($fp);//バッファを強制書き出し
        flock($fp, LOCK_UN);
        fclose($fp);
    }

    public function putNewFile($text, $file_name)
    {

        try {
            $fp = fopen($file_name, 'w');//新規作成可・オープン時に削除しない
        } catch (\Exception $e) {
            throw $e;
        }

        if (!flock($fp, LOCK_EX)) {
            throw new \Exception("ファイルロックできませんでした");
        }
        fwrite($fp, $text . "\n"); //ファイルの中身を出力
        fflush($fp);//バッファを強制書き出し
        flock($fp, LOCK_UN);
        fclose($fp);
    }
}
