<?php


namespace Eccube\ServiceProvider;

use Eccube\Form\Type\Admin\BannerType;
use Eccube\Form\Type\Admin\CampaignType;
use Eccube\Form\Type\Admin\ProductRecommendType;
use Eccube\Form\Type\Front\PointUseType;
use Eccube\Form\Type\Master\BannerPositionType;
use Eccube\Service\PointService;
use Silex\Application as BaseApplication;
use Silex\ServiceProviderInterface;

class MallServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param BaseApplication $app An Application instance
     */
    public function register(BaseApplication $app)
    {
        // Repository
        $app['eccube.repository.banner'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\Banner');
        });
        $app['eccube.repository.product_view_history'] = $app->share(function () use ($app) {
            $ProductViewHistoryRepository = $app['orm.em']->getRepository('\Eccube\Entity\ProductViewHistory');
            $ProductViewHistoryRepository->setApplication($app);
            return $ProductViewHistoryRepository;
        });
        $app['eccube.repository.aeonregi_transaction'] = $app->share(function () use ($app) {
            $ProductViewHistoryRepository = $app['orm.em']->getRepository('\Eccube\Entity\AeonregiTransaction');
            $ProductViewHistoryRepository->setApplication($app);
            return $ProductViewHistoryRepository;
        });


        $app['eccube.repository.master.maker'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Eccube\Entity\Master\Maker');
        });

        $app['eccube.repository.master.banner_position'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Eccube\Entity\Master\BannerPosition');
        });
        $app['eccube.repository.banner'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\Banner');
        });
        $app['eccube.repository.point_transaction'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\PointTransaction');
        });
        $app['eccube.repository.campaign'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\Campaign');
        });
        $app['eccube.repository.campaign_target_office'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\CampaignTargetOffice');
        });
        $app['eccube.repository.campaign_target_product'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\CampaignTargetProduct');
        });

        $app['eccube.repository.maker'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\Master\Maker');
        });
        $app['eccube.repository.constant'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\Constant');
        });
        $app['eccube.repository.product_ranking'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\ProductRanking');
        });
        $app['eccube.repository.real_estate'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\RealEstate');
        });

        $app['eccube.repository.office'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\Office');
        });

        // Form Type
        $app['form.types'] = $app->share($app->extend('form.types', function ($types) use ($app) {
            $types[] = new ProductRecommendType($app);
            $types[] = new BannerType($app);
            $types[] = new CampaignType($app);
            $types[] = new BannerPositionType();
            $types[] = new PointUseType($app);

            return $types;
        }));


        $app['shop_list'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\BaseInfo')->findAll();
        });
        $app['is_admin'] = $app->share(function () use ($app) {
            $isAdmin = false;
            if (php_sapi_name() !== "cli") {
                $path = preg_replace("/^\/index_dev.php/i", "", $_SERVER['REQUEST_URI']);
                if (strpos(rawurldecode($path), '/' . trim($app['config']['admin_route'], '/') . '/') === 0) {
                    $isAdmin = true;
                }
            }
            return $isAdmin;
        });
        $app['front_shop_id'] = $app->share(function () use ($app) {
            return 1;
        });

        $app['point_service'] = $app->share(function () use ($app) {
            return new PointService($app);
        });

        $app['admin_shop_id'] = $app->share(function () use ($app) {
            if ($app['session']->has('admin_shop_id'))
                return $app['session']->get('admin_shop_id');
            return 1;
        });
        $app['admin_current_shop'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\BaseInfo')->find($app['admin_shop_id']);
        });
        $app['front_current_shop'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('\Eccube\Entity\BaseInfo')->find($app['front_shop_id']);
        });
    }


    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * @param BaseApplication $app
     */
    public function boot(BaseApplication $app)
    {
    }
}
