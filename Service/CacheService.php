<?php
namespace Eccube\Service;

use Eccube\Application;

class CacheService
{
    private $path;

    /**
     *
     */
    public function __construct()
    {
        $app = Application::getInstance();
        $this->path = $app['config']['root_dir'] . '/app/cache/common/';
        if (!is_dir($this->path)) {
            mkdir($this->path, 0777, true);
        }
    }

    public function has($key)
    {
        return is_file($this->path . $this->generateFileName($key));
    }

    public function get($key)
    {
        if ($this->has($key)) {
            $text = unserialize(file_get_contents($this->path . $this->generateFileName($key)));
            $expiry_time = $text['expiry_time'];
            if ($expiry_time > 0 && $expiry_time < time()) {
                $this->destroy($key);
                return null;
            }
            return unserialize($text['data']);
        }
        return null;
    }

    public function set($key, $value, $minute = null)
    {
        $expiry_time = null;
        if ($minute > 0) {
            $expiry_time = strtotime('+' . $minute . 'minutes');
        }
        if (file_put_contents($this->path . $this->generateFileName($key), serialize([
            'data' => serialize($value),
            'expiry_time' => (int)$expiry_time
        ])))
            return true;
        return false;
    }

    public function destroy($key)
    {
        if ($this->has($key))
            if (unlink($this->path . $this->generateFileName($key)))
                return true;
        return false;
    }

    private function generateFileName($key)
    {
        return hash('sha256', $key);
    }

}