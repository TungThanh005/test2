<?php

namespace Eccube\Service;

use Eccube\Application;
use Eccube\Entity\Order;

class AeonregiService
{
    /** @var \Eccube\Application */
    public $app;

    /** @var \Eccube\Service\CartService */
    protected $cartService;

    /** @var \Eccube\Service\OrderService */
    protected $orderService;

    /** @var \Eccube\Entity\BaseInfo */
    protected $BaseInfo;

    /** @var  \Doctrine\ORM\EntityManager */
    protected $em;

    public function __construct(Application $app, $cartService, $orderService)
    {
        $this->app = $app;
        $this->cartService = $cartService;
        $this->orderService = $orderService;
        $this->BaseInfo = $app['eccube.repository.base_info']->get();
    }

    public function generateRequestParameter(Order $Order)
    {
        // 画面出力モード
        $dspmode = '01';//PC
        if ($this->app["mobile_detect"]->isMobile()) {
            $dspmode = '03';//スマホ
        }
        switch ($Order->getPayment()->getId()) {
            //クレジットカード決済
            case 1:
                $proc_id = '01';
                break;
            //銀行振込
            case 2:
                $proc_id = '02';
                break;
            //コンビニ決済
            case 3:
                $proc_id = '03';
                break;
            //定期便クレジットカード決済
            case 4:
                $proc_id = '01';
                break;
        }
        $base_url = trim($this->app->url('top'), "/");

        $param = array(
            "proc_id" => $proc_id,
            "auth_type" => "01",
            "dspmode" => $dspmode,
            "shop_id" => $Order->getShop()->getShopCode(),
            "order_id" => $Order->getId(),
            "total_amount" => $Order->getPaymentTotal(),
            "price_amount" => "0",
            "tax_amount" => "0",
            "shipping_amount" => "0",
            "pay_amount" => $Order->getPaymentTotal(),
            "point" => "0",
            "email" => $Order->getEmail(),
            "owners_flg" => "0",
            "success_url" => $base_url . config('aeonregi_payment_success') . '?_=' . $Order->getPreOrderId() . '&__=' . $Order->getId(),
            "failure_url" => $base_url . config('aeonregi_payment_failure') . '?_=' . $Order->getPreOrderId() . '&__=' . $Order->getId(),
            "cancel_url" => $base_url . config('aeonregi_payment_cancel') . '?_=' . $Order->getPreOrderId() . '&__=' . $Order->getId(),
            "card_type" => "00",
            //"prev_lidm"=>"00143075147237"
        );
        return [
            'action' => config('aeonregi_production_mode') == 1 ? config('aeonregi_payment_url') : config('aeonregi_test_payment_url'),
            'param' => $param
        ];
    }
}