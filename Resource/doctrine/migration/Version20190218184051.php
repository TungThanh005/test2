<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190218184051 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('INSERT INTO `mtb_csv_type` (`id`, `name`, `rank`) VALUES (\'6\', \'配送方法CSV\', \'6\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'id\', NULL, \'配送業者ID\', \'1\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'name\', NULL, \'配送業者名\', \'2\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'service_name\', NULL, \'名称\', \'3\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'confirm_url\', NULL, \'伝票No.URL\', \'4\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'ProductType\', \'id\', \'商品種別ID\', \'5\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'ProductType\', \'name\', \'商品種別名称\', \'6\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'description\', NULL, \'ショップ備考欄\', \'7\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'PaymentOptions\', \'Payment.id\', \'支払方法設定ID\', \'8\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'PaymentOptions\', \'Payment.method\', \'支払方法設定名称\', \'9\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, \'6\', \'1\', \'Eccube\\\\Entity\\\\Delivery\', \'DeliveryTimes\', \'Payment.delivery_time\', \'お届け時間設定\', \'10\', \'1\', \'2018-12-11 11:34:51\', \'2019-01-11 21:24:15\');
');
    }

    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM `dtb_csv` WHERE csv_type = 6;');
        $this->addSql('DELETE FROM `mtb_csv_type` WHERE id = 6;');
    }
}
